// @flow
// @jsx h
import type {
  CounterState,
  CounterActions
} from "./modules/counter/counter-actions";
import { h } from "hyperapp";
import {
  counterState,
  counterActions
} from "./modules/counter/counter-actions";
import CounterView from "./modules/counter/counter-view";
import About from "./modules/about/about-view";
import about from "./modules/about/about-actions";
import type { AboutActions, AboutState } from "./modules/about/about-actions";
import Layout from "./components/layout/layout";
import { Route } from "./modules/name-router/view";
import location from "./modules/name-router/actions";

type AppState = {
  counter: CounterState,
  location: Object,
  about: AboutState
};
const state: AppState = {
  counter: counterState,
  location: location.state,
  about: {
    ...about.state,
    data
  }
};

type AppActions = {
  counter: CounterActions,
  location: Object,
  about: AboutActions
};
const actions: AppActions = {
  counter: counterActions,
  location: location.actions,
  about: about.actions
};

type View = (AppState, AppActions) => Object;
const view: View = (state, actions) => (
  <div>
    <Route
      path="HOME"
      render={() => (
        <Layout>
          <CounterView count={state.counter.count} />
        </Layout>
      )}
    />
    <Route
      path="ABOUT"
      render={() => (
        <Layout>
          <About />
        </Layout>
      )}
    />
  </div>
);

export { view, actions, state };

export type { AppState, AppActions };
