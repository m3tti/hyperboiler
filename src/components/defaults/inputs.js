// @flow
// @jsx h
import { h } from "hyperapp";
import { style } from "./wrapper";

const Span = style("span")({
  width: "90%"
});

const Input = style("input")({
  width: "10%",
  transition: "width 0.5s linear",
  border: "none",
  "border-bottom": "1px solid black",
  ":focus": {
    width: "90%"
  }
});

const Label = style("label")({
  float: "left"
});

const InnerSpan = style("span")({});

const InputPrompt = ({ prompt, value, oninput }) => (
  <Span>
    <Input value={value} oninput={e => oninput(e.target.value)} />
    <Label>{prompt}</Label>
  </Span>
);

export { InputPrompt };
