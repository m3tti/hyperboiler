import { style } from "./wrapper";
import { keyframes } from "picostyle";

const fadeInAnimation = keyframes({
  from: {
    opacity: "0"
  },
  to: {
    opacity: "1"
  }
});

const fadeInDiv = style("div")({
  animation: fadeInAnimation + " 500ms"
});

export { fadeInDiv };
