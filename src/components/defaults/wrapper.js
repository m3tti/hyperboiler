// @flow
import { h } from "hyperapp";
import picostyle from "picostyle";

const style = picostyle(h);

export { style };
