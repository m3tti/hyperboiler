// @flow
import { h } from "hyperapp";
import icons from "@fortawesome/fontawesome-free-solid";

type IconProps = {
  iconName: ?string,
  iconPrefix: ?string,
  iconPath: ?string,
  viewBox: ?string,
  className: ?string,
  path: ?string,
  prefix: ?string,
  name: ?string
};

type Fa = (IconProps, string) => Object;
const fa: Fa = (props, iName) => {
  const {
    icon: [iconWidth, iconHeight, , , iconPath],
    iconName,
    prefix: iconPrefix
  } = icons[iName];

  const {
    name = iconName,
    className = "icon",
    viewBox = `0 0 ${iconWidth} ${iconHeight}`,
    prefix = iconPrefix,
    path = iconPath
  } = props;

  return h(
    "svg",
    {
      xmlns: "http://www.w3.org/2000/svg",
      ariaHidden: "true",
      role: "img",
      dataIcon: name,
      dataPrefix: prefix,
      className,
      viewBox
    },
    [h("path", { fill: "currentColor", d: path })]
  );
};

export default fa;
