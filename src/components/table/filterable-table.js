import { h } from "hyperapp";
import SimpleTable from "./simple-table";
import { where, filter } from "ramda";

type Props = {
  filter: Object,
  showKeys: [String],
  header: [String],
  elements: [Object],
  onclick: Function
};

type View = Props => Object;
const view: View = ({
  filter: filterObj,
  showKeys,
  header,
  elements,
  onclick
}) => (
  <SimpleTable
    elements={filter(where(filterObj), elements)}
    showKeys={showKeys}
    header={header}
    onclick={onclick}
  />
);

export default view;
