import { h } from "hyperapp";
import { style } from "../defaults/wrapper";
import { map, keys, head, props, values } from "ramda";

const td_th = {
  border: "1px solid #dddddd",
  "text-align": "left",
  padding: "8px"
};

const TableElement = style("td")(td_th);

const TableRow = style("tr")({
  ":nth-child(even)": {
    "background-color": "#dddddd"
  },
  ":hover": {
    "background-color": "steelblue"
  }
});

const TableHeader = style("th")(td_th);

const Table = style("table")({
  "font-family": "arial, sans-serif",
  "border-collapse": "collapse",
  width: "100%"
});

type Header = ([Object]) => Object;
const HeaderRow = ({ elements }) => (
  <TableRow>
    {map(
      d => (
        <TableHeader>{d}</TableHeader>
      ),
      elements
    )}
  </TableRow>
);

type DataRow = Function => Object => Object;
const DataRow: DataRow = ({ onclick, showKeys }, [data]) => (
  <TableRow onclick={() => onclick(data)}>
    {map(
      d => (
        <TableElement>{d}</TableElement>
      ),
      props(showKeys, data)
    )}
  </TableRow>
);

type Data = ([Object]) => Object;
const Content: Data = ({ showKeys, elements, onclick }) =>
  map(
    d => (
      <DataRow onclick={onclick} showKeys={showKeys}>
        {d}
      </DataRow>
    ),
    elements
  );

type Props = {
  showKeys: [String],
  header: [String],
  elements: [Object],
  onclick: Function
};

type SimpleTableT = Props => Object;
const SimpleTable: SimpleTableT = ({ showKeys, header, elements, onclick }) => (
  <Table>
    <HeaderRow elements={header} />
    <Content showKeys={showKeys} elements={elements} onclick={onclick} />
  </Table>
);

export default SimpleTable;
