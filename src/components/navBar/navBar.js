// @flow
import { h } from "hyperapp";
import { Link } from "../../modules/name-router/view";

const NavBar = () => (
  <div>
    <Link to="HOME">Main</Link>
    <Link to="ABOUT">About</Link>
  </div>
);

export default NavBar;
