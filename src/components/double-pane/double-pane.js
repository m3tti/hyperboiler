import { h } from "hyperapp";
import { style } from "../defaults/wrapper";
import { split, head, last, __, pipe, assoc, map, concat } from "ramda";

const Container = style("div")({
  display: "flex"
});

const LeftContainer = style("div")({
  "margin-right": "5px"
});

const RightContainer = style("div")({
  "margin-left": "5px"
});

const view = ({ splitRatio }, [left, right]) => {
  const ratio = split("/", splitRatio);
  const ratioStyles = map(
    pipe(
      concat(__, "%"),
      assoc("width", __, {})
    ),
    ratio
  );

  return (
    <Container>
      <LeftContainer style={head(ratioStyles)}>{left}</LeftContainer>
      <RightContainer style={last(ratioStyles)}>{right}</RightContainer>
    </Container>
  );
};

export default view;
