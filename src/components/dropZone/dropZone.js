// @flow
// @jsx h
import { h } from "hyperapp";
import { style } from "../defaults/wrapper";

const container = style("div")({
  width: "100%",
  height: "50%",
  "background-color": "black"
});

const Input = style("input")({
  "[type=file]": {
    cursor: "inherit",
    display: "block",
    opacity: "0",
    position: "absolute",
    right: "0",
    "text-align": "right"
  },
  width: "100%",
  height: "100%"
});

const view = ({ label, submit, onchange }) => (
  <container>
    <label>{label}</label>
    <Input type="file" id="file" onchange={e => onchange(e.target.files)} />
    <button onclick={submit}>submit</button>
  </container>
);

export default view;
