// @flow
import { h } from "hyperapp";
import { link } from "../defaults/wrapper";
import NavBar from "../navBar/navBar";
import { fadeInDiv } from "../../components/defaults/animaitons";

type View = Object => Object;
const view: View = (props, children) => (
  <div>
    <NavBar />
    <fadeInDiv>{children}</fadeInDiv>
  </div>
);

export default view;
