// @flow
import { h } from "hyperapp";
import { style } from "../defaults/wrapper";

const container = style("div")(props => {
  return {
    position: "absolute",
    width: "400px",
    padding: "10px",
    "font-size": "14px",
    "text-align": "center",
    color: "rgb(113, 157, 171)",
    background: "#2d2d2d",
    border: "4px solid #2d2d2d",
    "border-radius": "5px",
    "text-shadow": "rgba(0, 0, 0, 0.1) 1px 1px 1px",
    "box-shadow": "rgba(0, 0, 0, 0.1) 1px 1px 2px 0px",
    "::after": {
      content: "''",
      position: "absolute",
      width: "0",
      height: "0",
      "border-width": "10px",
      "border-style": "solid",
      "border-color": "transparent transparent #2d2d2d transparent",
      bottom: "102%",
      left: "10%"
    },
    transition: "visibility 0s, opacity 0.5s linear",
    visibility: props.open ? "block" : "hidden",
    opacity: props.open ? "1" : "0"
  };
});

export default container;
