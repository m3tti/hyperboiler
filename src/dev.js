// @flow
import { app } from "hyperapp";
import devtools from "hyperapp-redux-devtools";
import { view, actions, state } from "./app";

document.addEventListener("DOMContentLoaded", event => {
  const main = devtools(app)(state, actions, view, document.body);
});
