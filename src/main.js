// @flow
import { app } from "hyperapp";
import { view, actions, state } from "./app.js";
import { location } from "@hyperapp/router";

document.addEventListener("DOMContentLoaded", event => {
  const main = app(state, actions, view, document.body);
  const unsubscribe = location.subscribe(main.location);
});
