import { h } from "hyperapp";
import { style } from "../../components/defaults/wrapper";

const container = style("div")({});

const view = ({ data }) => (
  <container>
    <h3>{data.name}</h3>
  </container>
);

export default view;
