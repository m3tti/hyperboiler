//@flow
import type { AppState, AppActions } from "../../app";

type InfoViewData = {
  name: string,
  test: string
};

type AboutState = {
  open: boolean,
  infoViewData: InfoViewData,
  nameFilter: string
};
const state: AboutState = {
  open: false,
  infoViewData: {
    name: "",
    test: ""
  },
  nameFilter: "",
  fileLabel: "Upload File"
};

type Open = void => AboutState => AboutState;
const open: Open = () => state => ({ ...state, open: true });

type Close = void => AboutState => AboutState;
const close: Close = () => state => ({ ...state, open: false });

type UpdateInfoView = InfoViewData => AboutState => AboutState;
const updateInfoView: UpdateInfoView = infoViewData => state => {
  console.log(infoViewData);
  return { ...state, infoViewData };
};

type UpdateNameFilter = Object => AboutState => AboutState;
const updateNameFilter: UpdateNameFilter = nameFilter => state => ({
  ...state,
  nameFilter
});

const fileChange = file => state => {
  return { ...state, fileLabel: "Upload " + file[0].name };
};

type AboutActions = {
  open: Open,
  close: Close,
  updateInfoView: UpdateInfoView
};
const actions: AboutActions = {
  open,
  close,
  updateInfoView,
  updateNameFilter,
  fileChange
};

export default {
  state,
  actions
};

export type { AboutState, AboutActions, InfoViewData };
