// @flow
import { h } from "hyperapp";
import type { AppState, AppActions } from "../../app";
import Tooltip from "../../components/tooltip/view";
import { InputPrompt } from "../../components/defaults/inputs";
import FilterableTable from "../../components/table/filterable-table";
import SimpleTable from "../../components/table/simple-table";
import InfoView from "./info-view";
import DoublePane from "../../components/double-pane/double-pane";
import DropZone from "../../components/dropZone/dropZone";
import { contains } from "ramda";

type PropTypes = {};

type View = PropTypes => (AppState, AppActions) => Object;
const view: View = props => ({ about: state }, { about: actions }) => (
  <div>
    <h1 onmouseover={actions.open} onmouseleave={actions.close}>
      About
    </h1>

    <Tooltip open={state.open}>
      <h1>Hallo Welt</h1>
    </Tooltip>

    <InputPrompt
      prompt="Name: "
      value={state.nameFilter}
      oninput={actions.updateNameFilter}
    />

    <DoublePane splitRatio="60/40">
      <FilterableTable
        filter={{ name: contains(state.nameFilter) }}
        showKeys={["name", "vorname"]}
        header={["Name", "Vorname"]}
        elements={state.data}
        onclick={actions.updateInfoView}
      />

      <InfoView data={state.infoViewData} />
    </DoublePane>

    <DropZone
      submit={actions.submit}
      onchange={actions.fileChange}
      label={state.fileLabel}
    />
  </div>
);

export default view;
