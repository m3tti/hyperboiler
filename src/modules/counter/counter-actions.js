// @flow
type CounterState = {
  count: number
};
const counterState = {
  count: 0
};

type Down = number => CounterState => CounterState;
const down: Down = value => state => ({ ...state, count: state.count - value });

type Up = number => CounterState => CounterState;
const up: Up = value => state => ({ ...state, count: state.count + value });

type CounterActions = {
  up: Up,
  down: Down
};
const counterActions: CounterActions = {
  down,
  up
};

export { counterState, counterActions };

export type { CounterActions, CounterState };
