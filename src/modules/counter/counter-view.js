// @flow
import type { AppState, AppActions } from "../../app";
import { style } from "../../components/defaults/wrapper";
import { h } from "hyperapp";
import icon from "../../components/defaults/fontawesome";

const Text = style("h1")({
  fontSize: "30px",
  color: "black",
  margin: "100",
  transition: "transform .2s ease-out",
  ":hover": {
    transform: "scale(1.2)"
  }
});

type PropTypes = {
  count: number
};
type View = PropTypes => (AppState, AppActions) => Object;
const view: View = ({ count }) => (state, actions) => {
  const { counter: act } = actions;

  return (
    <div>
      <Text>{count}</Text>
      <button onclick={() => act.down(1)}> - </button>
      <button onclick={() => act.up(1)}> + </button>
    </div>
  );
};

export default view;
