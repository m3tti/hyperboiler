// @flow
import { prop } from "ramda";
import type { AppState } from "../../app";

const HOME = "HOME";

type LocationState = {
  current: string,
  last: string
};
const state: LocationState = {
  current: HOME,
  last: HOME
};

type Go = string => LocationState => LocationState;
const go: Go = path => state => ({ current: path, last: state.current });

type Redirect = void => LocationState => LocationState;
const redirect: Redirect = () => state => ({
  current: state.last,
  last: state.current
});

type LocationActions = { go: Go, redirect: Redirect };
const actions = { go, redirect };

type GetLocation = AppState => LocationState;
const getLocation: GetLocation = prop("location");

const selectors = {
  getLocation
};

export default {
  state,
  actions,
  selectors
};
