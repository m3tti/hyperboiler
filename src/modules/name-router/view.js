// @flow
import type { AppState, AppActions } from "../../app";
import { h } from "hyperapp";
import location from "./actions";

type LinkProps = {
  to: string
};

type LinkT = (LinkProps, string) => (AppState, AppActions) => Object;
const Link: LinkT = (props, label) => (state, actions) => (
  <a href="#" onclick={() => actions.location.go(props.to)}>
    {label}
  </a>
);

type RouteProps = {
  path: string,
  render: void => Object
};

type RouteT = RouteProps => (AppState, AppActions) => Object;
const Route: RouteT = props => (state, actions) => {
  const { current } = location.selectors.getLocation(state);
  if (props.path === current) {
    console.log("render " + current);
    return props.render();
  }

  return {};
};

export type { RouteProps, LinkProps };

export { Route, Link };
