default: serve
.PHONY: clean dist prettier jest flow flow-stop

setup:
	npm init --yes

	npm install --save hyperapp @hyperapp/fx @hyperapp/router picostyle ramda @fortawesome/fontawesome @fortawesome/fontawesome-free-solid

	npm install --save-dev browserify babelify watchify babel-core babel-preset-env babel-preset-es2015 babel-preset-flow browser-sync tinyify flow-typed flow-bin flow jest hyperapp-redux-devtools native-css babel-plugin-transform-object-rest-spread babel-plugin-transform-react-jsx prettier

	node_modules/.bin/flow-typed install

dist: node_modules/* src/*
	rm -rf dist/bundle.js
	node_modules/.bin/browserify -t babelify -p tinyify src/main.js > dist/bundle.js

serve: node_modules/* src/* 
	node_modules/.bin/watchify -d -t babelify src/dev.js -o dist/bundle.js &
	cd dist && ../node_modules/.bin/browser-sync start --server --files "*" --single

flow: node_modules/* src/*
	node_modules/.bin/flow status

flow-stop:
	node_modules/.bin/flow stop

jest: node_modules/* src/*
	node_modules/.bin/jest 

clean:
	rm -rf dist/bundle.js node_modules package.json package-lock.json flow-typed

prettier:
	node_modules/.bin/prettier --write "src/**/*.js"


